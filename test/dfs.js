// Tree node creating
import {TreeNode, Queue, Stack} from './utils.js';

const five = new TreeNode(5);

const six = new TreeNode(6);

const three = new TreeNode(3);
const two = new TreeNode(2);


const eight = new TreeNode(8);

const one = new TreeNode(1);

const seven = new TreeNode(7);
const ten = new TreeNode(10);

const nine = new TreeNode(9);
const eleven = new TreeNode(11);

// Tree node linking
five.children.push(six, eight, one);
six.children.push(three, two);
one.children.push(seven, ten);
ten.children.push(nine, eleven);

// DFS Traverse - Pre-order
function depthFirstSearch(root) {
  const stack = new Stack();
  stack.push(root);

  while (!stack.isEmpty()) {
    const node = stack.pop();

    console.log(node.data);

    [...node.children].reverse().forEach(child => stack.push(child));
  }
}

function breadthFirstSearch(root) {
  const queue = new Queue();
  queue.enqueue(root);

  while (!queue.isEmpty()) {
    const node = queue.dequeue();

    console.log(node.data);

    [...node.children].reverse().forEach(child => queue.enqueue(child));
  }
}
depthFirstSearch(five);
console.log('---------------------------------------------------------------------------------------------------------------------------');
breadthFirstSearch(five);