// insert(data) – It inserts a new node in a tree with a value data
// remove(data) – This function removes a node with a given data
// inOrder(node) – It performs in -order traversal of a tree starting from a given node.The implementation should be realized with recursion.
// preOrder(node) – It performs pre - order traversal of a tree starting from a given node.The implementation should be realized with recursion.
// postOrder(node) – It performs post - order traversal of a tree starting from a given node.The implementation should be realized with recursion.

import { TreeNode, Queue, Stack } from "./utils.js";

const five = new TreeNode(5);
const six = new TreeNode(6);
const three = new TreeNode(3);
const two = new TreeNode(2);
const eight = new TreeNode(8);
const one = new TreeNode(1);
const seven = new TreeNode(7);
const ten = new TreeNode(10);
const nine = new TreeNode(9);
const eleven = new TreeNode(11);

five.children.push(six, eight, one);
six.children.push(three, two);
one.children.push(seven, ten);
ten.children.push(nine, eleven);

class BST {
  constructor(node) {
    this.root = node;
  }

  remove(data, node = this.root) {
    if (!node) return;
    if (node.data === data) {
      console.log('data,node.data');
      node.children = [];
      node = null;
      return;
    }
    node.children.forEach((e) => {
      this.remove(data, e);
    });
  }

  inOrder(node = this.root) {
    if (!node) return;
    if (node === this.root) {
      console.log(node.data);
    }
    node.children.forEach((e) => {
      console.log(e.data);
      this.inOrder(e);
    });
  }
}

const tree = new BST(five);
tree.inOrder();
console.log('------------------------------');
tree.remove(11);
console.log('------------------------------');
tree.inOrder();
