/* Queue */
export class Queue {
  constructor() {
    this.items = [];
  }

  dequeue() {
    return this.items.shift();
  }

  enqueue(item) {
    this.items.push(item);
  }

  print() {
    while (!this.isEmpty()) {
      console.log(this.dequeue());
    }
  }

  isEmpty() {
    return this.items.length === 0;
  }
}

/* Stack */
export class Stack {
  constructor() {
    this.items = [];
  }

  pop() {
    return this.items.pop();
  }

  push(item) {
    this.items.push(item);
  }

  print() {
    while (!this.isEmpty()) {
      console.log(this.pop());
    }
  }

  isEmpty() {
    return this.items.length === 0;
  }
}

/* TreeNode */
export class TreeNode {
  constructor(data) {
    this.data = data;
    this.children = [];
  }
}