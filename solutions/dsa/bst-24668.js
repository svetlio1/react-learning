class TreeNode {
  constructor(data) {
    this.data = data;
    this.children = [];
  }
}

class Queue {
  constructor() {
    this.items = [];
  }
  dequeue() {
    return this.items.shift();
  }
  enqueue(item) {
    this.items.push(item);
  }
  print() {
    while (!this.isEmpty()) {
      console.log(this.dequeue());
    }
  }
  isEmpty() {
    return this.items.length === 0;
  }
}



class Stack {
  constructor() {
    this.items = [];
  }
  pop() {
    return this.items.pop();
  }
  push(item) {
    this.items.push(item);
  }
  print() {
    while (!this.isEmpty()) {
      console.log(this.pop());
    }
  }
  isEmpty() {
    return this.items.length === 0;
  }
}


// class BST
// insert(data) – It inserts a new node in a tree with a value data
// remove(data) – This function removes a node with a given data
// inOrder(node) – It performs in -order traversal of a tree starting from a given node.The implementation should be realized with recursion.
// preOrder(node) – It performs pre - order traversal of a tree starting from a given node.The implementation should be realized with recursion.
// postOrder(node) – It performs post - order traversal of a tree starting from a given node.The implementation should be realized with recursion.
class BST {
  constructor(node) {
    this.root = node;
  }

  getRootNode() {
    return this.root;
  }

  insert(data) {
    if (!this.root) {
      this.root = new TreeNode(data);
      return;
    }
    const queue = new Queue();
    queue.enqueue(this.root);
    while (!queue.isEmpty()) {
      const node = queue.dequeue();
      if (node.children.length < 2) {
        node.children.push(new TreeNode(data));
        break;
      } else {
        node.children.forEach((child) => queue.enqueue(child));
      }
    }
  }

  remove(data, node = this.root) {
    if (!data || !node) return;
    node.children = node.children.filter((el) => el.data !== data);
    node.children.forEach((el) => {
      this.remove(data, el);
    });
  }

  inOrder(node = this.root) {
    if (!node) return;
    if (node === this.root) {
      console.log(node.data);
    }
    node.children.forEach((e) => {
      console.log(e.data);
      this.inOrder(e);
    });
  }

  preOrder(node = this.root, que = new Queue()) {
    if (!node) return;
    if (node === this.root) {
      console.log(node.data);
    }
    node.children.forEach((e) => {
      console.log(e.data);
      que.enqueue(e);
    });
    const nextNode = que.dequeue();
    if (nextNode) this.preOrder(nextNode, que);
  }

  postOrder(node = this.root, queue = new Queue()) {
    if (!node) return;
    node.children.forEach((e) => {
      this.postOrder(e, queue);
      queue.enqueue(e.data);
    });
    if (node === this.root) {
      queue.enqueue(node.data);
    }
    queue.print();
  }
}

const tree = new BST();
let counter = 1;

while (counter <= 15) {
  tree.insert(counter);
  counter++;
}